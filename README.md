You must run two commands: "git submodule init" to
initialize your local configuration file, and "git
submodule update" to fetch all the data from that project
and check out the appropriate commit listed in your superproject

To run project:
docker-compose build
docker-compose up